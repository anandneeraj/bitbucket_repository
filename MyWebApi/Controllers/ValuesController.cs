﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyWebApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public string Get()
        {
            //previous return type IEnumerable<string>
            string json = JsonConvert.SerializeObject(new
            {
                results = new List<Result>()
                {
                    new Result { id = 1, value = "ABC", info = "ABC" },
                    new Result { id = 2, value = "JKL", info = "JKL" }
                }
            });

            string json1 = JsonConvert.SerializeObject(new
            {

            });
            
            return json;
            //return new string[] { "value1", "value2" };
        }

        public class Result
        {
            public int id { get; set; }
            public string value { get; set; }
            public string info { get; set; }
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
